let player = prompt('Dime tu nombre:', '');
let turno;
let pc;
let sumaJugador = 0;
let sumaPc = 0;
let sumaTurno = 1;
let jugada;

function tiraJugador(jugada) {

  
  if (jugada != 'piedra' && jugada != 'papel' && jugada != 'tijera') {
    
    alert('Debes elegir una de las tres opciones: piedra, papel o tijera');
    
  } else {
    
    return jugada;
  }
  
}

function tiraPc() {
  
  let options = ['piedra', 'papel', 'tijera'];
  let pcAzar = Math.round(Math.random() * (2));
  pc = options[pcAzar];
  return pc;
}


for (i = 1; sumaTurno<6 ; i++) {
  
  turno = prompt('Cual es tu jugada: piedra, papel o tijera').toLowerCase();
  document.write(`TURNO ${i}:<BR>`)

  if (tiraJugador(turno) == 'piedra' && tiraPc() == 'papel') {

    sumaPc++;
    sumaTurno;

  } else if (tiraJugador(turno) == 'piedra' && tiraPc() == 'tijera') {

    sumaJugador++;
    sumaTurno++;

  } else if (tiraJugador(turno) == 'papel' && tiraPc() == 'tijera') {

    sumaPc++;
    sumaTurno++;

  } else if (tiraJugador(turno) == 'papel' && tiraPc() == 'piedra') {

    sumaJugador++;
    sumaTurno++;

  } else if (tiraJugador(turno) == 'tijera' && tiraPc() == 'papel') {

    sumaJugador++;
    sumaTurno++;

  } else if (tiraJugador(turno) == 'tijera' && tiraPc() == 'piedra') {

    sumaPc++;
    sumaTurno++;

  } else {
    
    document.write(`En esta jugada hubo empate<br>`);
    sumaTurno;
    continue;

  }

  document.write(`${player} eligió ${jugada} y el Pc eligió ${pc}<br>`);
  document.write(`El marcador está Jugador = ${sumaJugador}  -  Pc = ${sumaPc} <br><br>`);
}

if (sumaJugador > sumaPc) {

  document.write(`El ganador es ${player}`);
} else {
  document.write(`El ganador es el Pc`);
}